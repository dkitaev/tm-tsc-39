package ru.tsc.kitaev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO users" +
            " (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)" +
            " VALUES (#{user.id}, #{user.login}, #{user.passwordHash}, #{user.email}, #{user.firstName}," +
            " #{user.lastName}, #{user.middleName}, #{user.role}, #{user.locked})")
    void add(
            @NotNull @Param("user") User user
    );

    @Delete("DELETE FROM users")
    void clear();

    @Select("SELECT * FROM users")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @NotNull
    List<User> findAll();

    @Select("SELECT * FROM users ORDER BY #{sort}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @NotNull
    List<User> findAllBySort(
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM users WHERE id = #{id}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @Nullable
    User findById(
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM users LIMIT 1 OFFSET #{index}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @NotNull
    User findByIndex(
            @NotNull @Param("index") Integer index
    );

    @Delete("DELETE FROM users WHERE id = #{id}")
    void removeById(
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM users WHERE id =" +
            " (SELECT id FROM users LIMIT 1 OFFSET #{index})")
    void removeByIndex(
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT count(*) AS count FROM users")
    int getSize();

    @Select("SELECT * FROM users WHERE login = #{login}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @NotNull
    User findByLogin(
            @NotNull @Param("login") String login
    );

    @Select("SELECT * FROM users WHERE email = #{email}")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "first_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "middle_name", property = "middleName")
    @NotNull
    User findByEmail(
            @NotNull @Param("email") String email
    );

    @Delete("DELETE FROM users WHERE login = #{login}")
    void removeByLogin(
            @NotNull @Param("login") String login
    );

    @Update("UPDATE users SET password_hash = #{passwordHash} WHERE id = #{id}")
    void updatePassword(
            @NotNull @Param("id") String id,
            @NotNull @Param("passwordHash") String passwordHash
    );

    @Update("UPDATE users SET locked = #{locked} WHERE login = #{login}")
    void updateLockUserByLogin(
            @NotNull @Param("login") String login,
            @Param("locked") boolean locked
    );

    @Update("UPDATE users" +
            " SET first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName}" +
            " WHERE id = #{id}")
    void updateUser(
            @NotNull @Param("id") String id,
            @NotNull @Param("firstName") String firstName,
            @NotNull @Param("lastName") String lastName,
            @NotNull @Param("middleName") String middleName
    );

}
