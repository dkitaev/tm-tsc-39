package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @Nullable
    @WebMethod
    User findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    );

    @Nullable
    @WebMethod
    User findByEmail(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    );

    @WebMethod
    void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    );

    @NotNull
    @WebMethod
    User createUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    );

    @WebMethod
    void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    );

    @WebMethod
    void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    );

}
