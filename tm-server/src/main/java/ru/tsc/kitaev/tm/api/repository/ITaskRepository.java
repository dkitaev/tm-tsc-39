package ru.tsc.kitaev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tasks" +
            " (id, name, description, user_id, status, created, start_date, project_id)" +
            " VALUES (#{task.id}, #{task.name}, #{task.description}, #{task.userId}, #{task.status}, #{task.created}," +
            " #{task.startDate}, #{task.projectId})")
    void add(
            @NotNull @Param("task") Task task
    );

    @Delete("DELETE FROM tasks")
    void clear();

    @Delete("DELETE FROM tasks WHERE user_id = #{userId}")
    void clearByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tasks")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<Task> findAll();

    @Select("SELECT * FROM tasks WHERE user_id = #{userId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tasks WHERE user_id = #{userId} ORDER BY #{sort}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<Task> findAllBySort(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND id = #{id}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @Nullable
    Task findById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tasks WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    Task findByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Delete("DELETE FROM tasks WHERE user_id = #{userId} AND id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM tasks WHERE id =" +
            " (SELECT id FROM tasks WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void removeByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT count(*) AS count FROM tasks WHERE user_id = #{userId}")
    @NotNull
    Integer getSize(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND name = #{name}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    Task findByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Delete("DELETE FROM tasks WHERE user_id = #{userId} AND name = #{name}")
    void removeByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    );

    @Update("UPDATE tasks SET name = #{name}, description = #{description}" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void updateById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE tasks SET name = #{name}, description = #{description}" +
            " WHERE id = (SELECT id FROM tasks WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void updateByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("name") String name,
            @NotNull @Param("description") String description
    );

    @Update("UPDATE tasks SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND id = #{id}")
    void startById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE id = (SELECT id FROM tasks WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void startByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status}, start_date = CURRENT_TIMESTAMP" +
            " WHERE user_id = #{userId} AND name = #{name}")
    void startByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND id = #{id}")
    void finishById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status}" +
            " WHERE id = (SELECT id FROM tasks WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void finishByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND name = #{name}")
    void finishByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND id = #{id}")
    void changeStatusById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status}" +
            " WHERE id = (SELECT id FROM tasks WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void changeStatusByIndex(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("index") Integer index,
            @NotNull @Param("status") String status
    );

    @Update("UPDATE tasks SET status = #{status} WHERE user_id = #{userId} AND name = #{name}")
    void changeStatusByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name,
            @NotNull @Param("status") String status
    );

    @Select("SELECT * FROM tasks WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Result(column = "user_id", property = "userId")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "project_id", property = "projectId")
    @NotNull
    List<Task> findAllTaskByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Update("UPDATE tasks SET project_id = #{projectId} WHERE user_id = #{userId} AND id = #{id}")
    void bindTaskToProjectById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("id") String taskId
    );

    @Update("UPDATE tasks SET project_id = NULL"
            + " WHERE project_id = #{projectId} AND user_id = #{userId} AND id = #{id}")
    void unbindTaskById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("id") String taskId
    );

    @Delete("DELETE FROM tasks WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllTaskByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

}
