package ru.tsc.kitaev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO sessions (id, user_id, time_stamp, signature)" +
            " VALUES (#{session.id}, #{session.userId}, #{session.timestamp}, #{session.signature})")
    void add(
            @NotNull @Param("session") Session session
    );

    @Delete("DELETE FROM sessions")
    void clear();

    @Select("SELECT * FROM sessions")
    @Result(column = "time_stamp", property = "timestamp")
    @NotNull
    List<Session> findAll();

    @Select("SELECT * FROM sessions ORDER BY #{sort}")
    @Result(column = "time_stamp", property = "timestamp")
    @NotNull
    List<Session> findAllBySort(
            @NotNull @Param("sort") String sort
    );

    @Select("SELECT * FROM sessions WHERE id = #{id}")
    @Result(column = "start_date", property = "startDate")
    @Nullable
    Session findById(
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM sessions LIMIT 1 OFFSET #{index}")
    @Result(column = "start_date", property = "startDate")
    @NotNull
    Session findByIndex(
            @NotNull @Param("index") Integer index
    );

    @Delete("DELETE FROM sessions WHERE id = #{id}")
    void removeById(
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM sessions WHERE id =" +
            " (SELECT id FROM sessions LIMIT 1 OFFSET #{index})")
    void removeByIndex(
            @NotNull @Param("index") Integer index
    );

    @Select("SELECT count(*) AS count FROM sessions")
    int getSize();

}
